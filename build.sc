import mill._
import mill.scalalib._

object course_work extends ScalaModule {
  def scalaVersion = "2.12.6"

  val mainDeps = Agg(
    ivy"io.circe::circe-generic-extras:0.10.0-M1",
    ivy"de.heikoseeberger::akka-http-circe:1.21.0",
    ivy"com.typesafe.akka::akka-http:10.1.1")

  override def ivyDeps = mainDeps

  override def finalMainClass = "wtf.scala.coursetest.naive.Server"

  override def scalacPluginIvyDeps = super.scalacPluginIvyDeps() ++ Agg(
    ivy"org.scalamacros:::paradise:2.1.1"
  )

  object test extends Tests {
    def testFrameworks = Seq("org.scalatest.tools.Framework")

    override def ivyDeps = mainDeps ++ Agg(ivy"com.github.agourlay::cornichon-scalatest:0.16.0")
  }
}
