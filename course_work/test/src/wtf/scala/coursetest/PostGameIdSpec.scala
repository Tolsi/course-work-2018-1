package wtf.scala.coursetest

import com.github.agourlay.cornichon.CornichonFeature
import com.github.agourlay.cornichon.core.FeatureDef
import io.circe.syntax._
import scala.concurrent.duration._
import wtf.scala.coursetest.TicTacToeJson._

class PostGameIdSpec extends CornichonFeature with TicTacToeSpecCommon {

  /**
    * \\| 0 1 2 3 4 5
    * ---------------
    * 0 | 2 0 0 0 0 1
    * 1 | 2 0 0 0 1 0
    * 2 | 0 0 0 1 0 0
    */
  def play_creator_win_game_6_x_3(winner: User, opponent: User) = Attach {
    When I create_user(winner)
    Then asserts status.is(200)
    And I create_user(opponent)
    Then asserts status.is(200)
    Then I login(winner)
    Then asserts status.is(200)
    Then I create_game(CreateGameRequest(opponent.username, FieldSize(6, 3), winner.username, 3))
    Then asserts status.is(200)
    Then asserts body.path("id").is(1)
    Then I stepAs(winner, 1, Step(5, 0))
    Then asserts status.is(200)
    Then I stepAs(opponent, 1, Step(0, 0))
    Then asserts status.is(200)
    Then I stepAs(winner, 1, Step(4, 1))
    Then asserts status.is(200)
    Then I stepAs(opponent, 1, Step(0, 1))
    Then asserts status.is(200)
    Then I stepAs(winner, 1, Step(3, 2))
    Then asserts status.is(200)
    Then asserts body.is(Game(1, None, Some(winner.username), finished = true, Seq(winner.username, opponent.username), 5, FieldSize(6, 3), 3,
      Field(
        Seq(
          Seq[Byte](2, 0, 0, 0, 0, 1),
          Seq[Byte](2, 0, 0, 0, 1, 0),
          Seq[Byte](0, 0, 0, 1, 0, 0)
        )
      )).asJson)
  }

  /**
    * \\| 0 1 2 3
    * ---------------------
    * 0 | 2 0 0 0
    * 1 | 0 2 0 0
    * 2 | 0 0 2 0
    * 3 | 1 0 0 0
    * 4 | 0 1 0 0
    * 5 | 0 0 1 0
    * 6 | 0 0 0 1
    * 7 | 0 0 0 0
    * 8 | 0 0 0 0
    */
  def play_creator_win_game_4_x_9(winner: User, opponent: User) = Attach {
    When I create_user(winner)
    Then asserts status.is(200)
    And I create_user(opponent)
    Then asserts status.is(200)
    Then I login(winner)
    Then asserts status.is(200)
    Then I create_game(CreateGameRequest(opponent.username, FieldSize(4, 9), winner.username, 4))
    Then asserts status.is(200)
    Then asserts body.path("id").is(1)
    Then I stepAs(winner, 1, Step(0, 3))
    Then asserts status.is(200)
    Then I stepAs(opponent, 1, Step(0, 0))
    Then asserts status.is(200)
    Then I stepAs(winner, 1, Step(1, 4))
    Then asserts status.is(200)
    Then I stepAs(opponent, 1, Step(1, 1))
    Then asserts status.is(200)
    Then I stepAs(winner, 1, Step(2, 5))
    Then asserts status.is(200)
    Then I stepAs(opponent, 1, Step(2, 2))
    Then asserts status.is(200)
    Then I stepAs(winner, 1, Step(3, 6))
    Then asserts status.is(200)
    Then asserts body.is(Game(1, None, Some(winner.username), finished = true, Seq(winner.username, opponent.username), 7, FieldSize(4, 9), 4,
      Field(
        Seq(
          Seq[Byte](2, 0, 0, 0),
          Seq[Byte](0, 2, 0, 0),
          Seq[Byte](0, 0, 2, 0),
          Seq[Byte](1, 0, 0, 0),
          Seq[Byte](0, 1, 0, 0),
          Seq[Byte](0, 0, 1, 0),
          Seq[Byte](0, 0, 0, 1),
          Seq[Byte](0, 0, 0, 0),
          Seq[Byte](0, 0, 0, 0)
        )
      )).asJson)
  }


  def play_draw_game_3_x_3(user1: User, user2: User) = Attach {
    When I create_user(user1)
    And I create_user(user2)
    Then I login(user1)
    Then I create_game(CreateGameRequest(user2.username, FieldSize(3, 3), user1.username, 3))
    Then asserts status.is(200)
    Then asserts body.path("id").is(1)
    Then I stepAs(user1, 1, Step(1, 1))
    Then asserts status.is(200)
    Then I stepAs(user2, 1, Step(0, 1))
    Then asserts status.is(200)
    Then I stepAs(user1, 1, Step(0, 0))
    Then asserts status.is(200)
    Then I stepAs(user2, 1, Step(2, 2))
    Then asserts status.is(200)
    Then I stepAs(user1, 1, Step(2, 1))
    Then asserts status.is(200)
    Then I stepAs(user2, 1, Step(2, 0))
    Then asserts status.is(200)
    Then I stepAs(user1, 1, Step(1, 0))
    Then asserts status.is(200)
    Then I stepAs(user2, 1, Step(1, 2))
    Then asserts status.is(200)
    Then I stepAs(user1, 1, Step(0, 2))
    Then asserts status.is(200)
    Then asserts body.is(Game(1, None, None, finished = true, Seq(user1.username, user2.username), 9, FieldSize(3, 3), 3,
      Field(
        Seq(
          Seq[Byte](1, 1, 2),
          Seq[Byte](2, 1, 1),
          Seq[Byte](1, 2, 2)
        )
      )).asJson)
  }

  /**
    * 2 1 0
    * 0 1 0
    * 2 1 0
    */
  def play_creator_win_game_3_x_3(winner: User, opponent: User) = Attach {
    When I create_user(winner)
    Then asserts status.is(200)
    And I create_user(opponent)
    Then asserts status.is(200)
    Then I login(winner)
    Then asserts status.is(200)
    Then I create_game(CreateGameRequest(opponent.username, FieldSize(3, 3), winner.username, 3))
    Then asserts status.is(200)
    Then asserts body.path("id").is(1)
    Then I stepAs(winner, 1, Step(1, 1))
    Then asserts status.is(200)
    Then I stepAs(opponent, 1, Step(0, 0))
    Then asserts status.is(200)
    Then asserts body.is(Game(1, Some(winner.username), None, finished = false, Seq(winner.username, opponent.username), 2, FieldSize(3, 3), 3,
      Field(
        Seq(
          Seq[Byte](2, 0, 0),
          Seq[Byte](0, 1, 0),
          Seq[Byte](0, 0, 0)
        )
      )).asJson)
    Then I stepAs(winner, 1, Step(1, 0))
    Then asserts status.is(200)
    Then I stepAs(opponent, 1, Step(0, 2))
    Then asserts status.is(200)
    Then I stepAs(winner, 1, Step(1, 2))
    Then asserts status.is(200)
    Then asserts body.is(Game(1, None, Some(winner.username), finished = true, Seq(winner.username, opponent.username), 5, FieldSize(3, 3), 3,
      Field(
        Seq(
          Seq[Byte](2, 1, 0),
          Seq[Byte](0, 1, 0),
          Seq[Byte](2, 1, 0)
        )
      )).asJson)
  }

  /**
    * 2 2 2
    * 1 1 0
    * 0 0 1
    */
  def play_opponent_win_game_3_x_3(creator: User, opponentWinner: User) = Attach {
    When I create_user(creator)
    Then asserts status.is(200)
    And I create_user(opponentWinner)
    Then asserts status.is(200)
    Then I login(creator)
    Then asserts status.is(200)
    Then I create_game(CreateGameRequest(opponentWinner.username, FieldSize(3, 3), creator.username, 3))
    Then asserts status.is(200)
    Then asserts body.path("id").is(1)
    Then I stepAs(creator, 1, Step(1, 1))
    Then asserts body.is(Game(1, Some(opponentWinner.username), None, finished = false, Seq(creator.username, opponentWinner.username), 1, FieldSize(3, 3), 3,
      Field(
        Seq(
          Seq[Byte](0, 0, 0),
          Seq[Byte](0, 1, 0),
          Seq[Byte](0, 0, 0)
        )
      )).asJson)
    Then asserts status.is(200)
    Then I stepAs(opponentWinner, 1, Step(0, 0))
    Then asserts status.is(200)
    Then I stepAs(creator, 1, Step(0, 1))
    Then asserts body.is(Game(1, Some(opponentWinner.username), None, finished = false, Seq(creator.username, opponentWinner.username), 3, FieldSize(3, 3), 3,
      Field(
        Seq(
          Seq[Byte](2, 0, 0),
          Seq[Byte](1, 1, 0),
          Seq[Byte](0, 0, 0)
        )
      )).asJson)
    Then asserts status.is(200)
    Then I stepAs(opponentWinner, 1, Step(2, 0))
    Then asserts status.is(200)
    Then I stepAs(creator, 1, Step(2, 2))
    Then asserts status.is(200)
    Then I stepAs(opponentWinner, 1, Step(1, 0))
    Then asserts status.is(200)
    Then asserts body.is(Game(1, None, Some(opponentWinner.username), finished = true, Seq(creator.username, opponentWinner.username), 6, FieldSize(3, 3), 3,
      Field(
        Seq(
          Seq[Byte](2, 2, 2),
          Seq[Byte](1, 1, 0),
          Seq[Byte](0, 0, 1)
        )
      )).asJson)
  }

  override def feature: FeatureDef = Feature("POST /game/:id") {
    Scenario("Returns 401 without 'session' header") {
      When I create_test_users_and_login_as_test_and_create_test_games
      Then I post("/game/1").withBody(Step(0, 0).asJson)
      Then asserts status.is(401)
    }

    Scenario("Returns 401 if 'session' header token has expired") {
      When I create_test_users_and_login_as_test_and_create_test_games
      Then I wait(11 seconds)
      Then I step(1, Step(0, 0))
      Then asserts status.is(401)
    }

    Scenario("Returns 401 if 'session' header token is invalid") {
      When I create_test_users_and_login_as_test_and_create_test_games
      Then I post("/game/1").withBody(Step(0, 0).asJson).withHeaders("session" -> "123")
      Then asserts status.is(401)
    }

    Scenario("Returns 403 if the user does not have the right to step in this game") {
      When I create_test_users_and_login_as_test_and_create_test_games
      When I logout
      Then asserts status.is(200)
      When I login(User("test3", "q" * 8))
      Then asserts status.is(200)
      Then I step(1, Step(0, 0))
      Then asserts status.is(403)
    }

    Scenario("Returns 404 for steping in not existed game") {
      When I create_test_users_and_login_as_test_and_create_test_games
      Then I step(100, Step(0, 0))
      Then asserts status.is(404)
    }

    Scenario("Returns 400 if the field is less than the step coordinates") {
      When I create_test_users_and_login_as_test_and_create_test_games
      Then I step(1, Step(3, 2))
      Then asserts status.is(400)
      Then I step(1, Step(2, 3))
      Then asserts status.is(400)
      Then I step(1, Step(3, 3))
      Then asserts status.is(400)
    }

    Scenario("Returns 400 if the step coordinates are nagative") {
      When I create_test_users_and_login_as_test_and_create_test_games
      Then I step(1, Step(-2, 2))
      Then asserts status.is(400)
      Then I step(1, Step(2, -1))
      Then asserts status.is(400)
      Then I step(1, Step(-1, -1))
      Then asserts status.is(400)
    }

    Scenario("Returns 400 if the game is over") {
      When I play_creator_win_game_3_x_3(User("test", "q" * 8), User("test2", "q" * 8))
      Then I stepAs(User("test2", "q" * 8), 1, Step(2, 2))
      Then asserts status.is(400)
    }

    Scenario("Returns 400 if someone else's turn is") {
      When I create_test_users_and_login_as_test_and_create_test_games
      When I logout
      Then asserts status.is(200)
      When I stepAs(User("test2", "q" * 8), 1, Step(0, 0))
      Then asserts status.is(400)
    }


    Scenario("Returns 400 if data does not meet the specification") {
      When I create_test_users_and_login_as_test_and_create_test_games
      Then I post("/game/1").withBody("{\"a\": \"b\"}").withHeaders("session" -> "<session>")
      Then asserts status.is(400)
    }

    Scenario("Returns 200 a draw case") {
      When I play_draw_game_3_x_3(User("test", "q" * 8), User("test2", "q" * 8))
      Then asserts status.isSuccess
    }

    Scenario("Returns 200 a 1 win case") {
      When I play_creator_win_game_3_x_3(User("test", "q" * 8), User("test2", "q" * 8))
      Then asserts status.isSuccess
    }

    Scenario("Returns 200 a 2 win case") {
      When I play_opponent_win_game_3_x_3(User("test", "q" * 8), User("test2", "q" * 8))
      Then asserts status.isSuccess
    }

    Scenario("Returns 200 a 3 win case") {
      When I play_creator_win_game_6_x_3(User("test", "q" * 8), User("test2", "q" * 8))
      Then asserts status.isSuccess
    }

    Scenario("Returns 200 a 4x9 win case") {
      When I play_creator_win_game_4_x_9(User("test", "q" * 8), User("test2", "q" * 8))
      Then asserts status.isSuccess
    }
  }
}
