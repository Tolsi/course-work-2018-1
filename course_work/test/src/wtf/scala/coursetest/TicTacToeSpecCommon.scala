package wtf.scala.coursetest

import com.github.agourlay.cornichon.CornichonFeature
import com.github.agourlay.cornichon.http.HttpRequest
import com.github.agourlay.cornichon.steps.regular.EffectStep
import io.circe.Printer
import io.circe.syntax._
import wtf.scala.coursetest.TicTacToeJson._

trait TicTacToeSpecCommon {
  self: CornichonFeature =>

  implicit val jsonPrinter = Printer.noSpaces.copy(dropNullValues = true)

  override lazy val baseUrl: String = "http://localhost:8080"

  def create_user(user: User) = EffectStep(
    title = s"create new user $user",
    effect = http.requestEffect(
      request = HttpRequest.post("/user").withBody(user.asJson)
    )
  )

  def create_game(createGameRequest: CreateGameRequest) = EffectStep(
    title = s"create new game $createGameRequest",
    effect = http.requestEffect(
      request = HttpRequest.post("/game").withBody(createGameRequest.asJson).withHeaders("session" -> "<session>")
    )
  )

  def reset_data = EffectStep(
    title = "reset the data",
    effect = http.requestEffect(
      request = HttpRequest.post("/debug/reset").withHeaders("admin" -> "any"),
      expectedStatus = Some(200)
    )
  )

  def login(user: User) = Attach {
    When I post("/user/login").withBody(user.asJson)
    And `save session` save_body_path("session" -> "session")
  }

  def get_games(limit: Int, offset: Int) = EffectStep(
    title = s"get $limit games from $offset",
    effect = http.requestEffect(
      request = HttpRequest.get("/game").withParams("limit" -> limit.toString, "offset" -> offset.toString)
    )
  )

  def get_users(limit: Int, offset: Int) = EffectStep(
    title = s"get $limit users from $offset",
    effect = http.requestEffect(
      request = HttpRequest.get("/user").withParams("limit" -> limit.toString, "offset" -> offset.toString)
    )
  )

  def step(game: Int, step: StepRequest) = EffectStep(
    title = s"step ${step.step} in game $game",
    effect = http.requestEffect(
      request = HttpRequest.post(s"/game/$game").withBody(step.asJson).withHeaders("session" -> "<session>")
    )
  )

  def logout = EffectStep(
    title = s"logout",
    effect = http.requestEffect(
      request = HttpRequest.post("/user/logout").withHeaders("session" -> "<session>")
    )
  )

  def create_test_users_and_login_as_test = Attach {
    When I create_user(User("test", "q" * 8))
    Then assert status.isSuccess
    When I create_user(User("test2", "q" * 8))
    Then assert status.isSuccess
    When I create_user(User("test3", "q" * 8))
    Then assert status.isSuccess
    And `login as user 'test'` login(User("test", "q" * 8))
    Then assert status.isSuccess
  }

  def stepAs(user: User, game: Int, s: StepRequest) = Attach {
    Then I logout
    Then `login as user 'test'` login(user)
    And assert status.isSuccess
    Then I step(game, s)
  }

  def create_many_test_users = Attach {
    When I create_user(User("test", "q" * 8))
    Then assert status.isSuccess
    When I create_user(User("test2", "q" * 8))
    Then assert status.isSuccess
    When I create_user(User("test3", "q" * 8))
    Then assert status.isSuccess
    When I create_user(User("test4", "q" * 8))
    Then assert status.isSuccess
    When I create_user(User("test5", "q" * 8))
    Then assert status.isSuccess
    When I create_user(User("test6", "q" * 8))
    Then assert status.isSuccess
  }

  def create_test_users_and_login_as_test_and_create_test_games = Attach {
    When I create_test_users_and_login_as_test
    Repeat(10) {
      Then `create game` create_game(CreateGameRequest("test2", FieldSize(3, 3), "test", 3))
      Then assert status.isSuccess
    }
  }

  def resetScenario = Attach {
    When I reset_data
    Then assert status.isSuccess
  }

  beforeEachScenario(resetScenario)
  afterEachScenario(resetScenario)
}
