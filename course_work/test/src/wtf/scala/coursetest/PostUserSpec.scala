package wtf.scala.coursetest

import com.github.agourlay.cornichon.CornichonFeature
import com.github.agourlay.cornichon.core.FeatureDef

class PostUserSpec extends CornichonFeature with TicTacToeSpecCommon {

  def feature: FeatureDef = Feature("POST /user") {

    Scenario("Returns 200 if user is valid and it was created") {
      When I create_user(User("test", "q" * 8))
      Then assert status.isSuccess
    }

    Scenario("Returns 200 if user is valid and contains sql injection data") {
      When I create_user(User("test", "'; sqllllllllll"))
      Then assert status.isSuccess
    }

    Scenario("Returns 409 if user with this username already exists") {
      When I create_user(User("test", "q" * 8))
      Then assert status.isSuccess
      And `try create this user again` create_user(User("test", "q" * 8))
      Then assert status.is(409)
    }

    Scenario("Returns 400 if user's 'username' length is less than 3") {
      When I create_user(User("te", "q" * 8))
      Then assert status.is(400)
    }

    Scenario("Returns 400 if user's 'username' length is greater than 19") {
      When I create_user(User("t" * 20, "q" * 8))
      Then assert status.is(400)
    }

    Scenario("Returns 400 if user's 'password' length is less than 8") {
      When I create_user(User("test", "q" * 7))
      Then assert status.is(400)
    }

    Scenario("Returns 400 if user's 'password' length is greater than 99") {
      When I create_user(User("test", "q" * 100))
      Then assert status.is(400)
    }
  }
}