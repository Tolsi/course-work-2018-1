package wtf.scala.coursetest

import com.github.agourlay.cornichon.CornichonFeature
import com.github.agourlay.cornichon.core.FeatureDef
import io.circe.Json

class PostDebugReset extends CornichonFeature with TicTacToeSpecCommon {

  def feature: FeatureDef = Feature("POST /debug/reset") {

    Scenario("Returns 200 if the data storage was dropped") {
      When I reset_data
      Then `the games data should be empty` get_games(100, 0)
      Then asserts status.isSuccess
      Then asserts body.is(Json.arr())
      Then `the users data should be empty` get_users(100, 0)
      Then asserts status.isSuccess
      Then asserts body.is(Json.arr())
      Then `the login as test user` create_test_users_and_login_as_test
      Then `the created game should have id = 1` create_game(CreateGameRequest("test2", FieldSize(3, 3), "test", 3))
      Then asserts status.isSuccess
      Then asserts body.path("id").is(1)
    }

    Scenario("Returns 401 if there are no 'admin' header") {
      When I post("/debug/reset")
      Then assers status.is(401)
    }

    Scenario("Returns 405 if it's not post request") {
      When I get("/debug/reset").withHeaders("admin" -> "any")
      Then assers status.is(405)
    }
  }
}