package wtf.scala.coursetest

import com.github.agourlay.cornichon.CornichonFeature
import com.github.agourlay.cornichon.core.FeatureDef

import io.circe.syntax._
import scala.concurrent.duration._

class PostGameSpec extends CornichonFeature with TicTacToeSpecCommon {
  override def feature: FeatureDef = Feature("POST /game") {
    Scenario("Returns 401 without 'session' header") {
      When I post("/game").withBody(CreateGameRequest("test2", FieldSize(3, 3), "test2", 3).asJson)
      Then assert status.is(401)
    }

    Scenario("Returns 401 if 'session' header token has expired") {
      When I create_test_users_and_login_as_test
      Then `wait 11secs` wait(11 seconds)
      And `create a game with inactive session` create_game(CreateGameRequest("test2", FieldSize(3, 3), "test2", 3))
      Then assert status.is(401)
    }

    Scenario("Returns 401 if 'session' header token is invalid") {
      When I post("/game").withBody(CreateGameRequest("test2", FieldSize(3, 3), "test2", 3).asJson).withHeaders("session" -> "gseg4")
      Then assert status.is(401)
    }

    Scenario("Returns 400 if 'opponent' user is not exists") {
      When I create_test_users_and_login_as_test
      And `create a game` create_game(CreateGameRequest("test1111", FieldSize(3, 3), "test", 3))
      Then assert status.is(400)
    }

    Scenario("Returns 400 if 'opponent' user is self") {
      When I create_test_users_and_login_as_test
      And `create a game` create_game(CreateGameRequest("test", FieldSize(3, 3), "test", 3))
      Then assert status.is(400)
    }

    Scenario("Returns 400 if 'firstStepBy' user is not exists") {
      When I create_test_users_and_login_as_test
      And `create a game` create_game(CreateGameRequest("test2", FieldSize(3, 3), "test1111", 3))
      Then assert status.is(400)
    }

    Scenario("Returns 400 if 'firstStepBy' user is not in the game") {
      When I create_test_users_and_login_as_test
      And `create a game` create_game(CreateGameRequest("test2", FieldSize(3, 3), "test3", 3))
      Then assert status.is(400)
    }

    Scenario("Returns 400 if 'size' x dimension is less than 3") {
      When I create_test_users_and_login_as_test
      And `create a game` create_game(CreateGameRequest("test2", FieldSize(2, 3), "test2", 2))
      Then assert status.is(400)
    }


    Scenario("Returns 400 if 'size' y dimension is less than 3") {
      When I create_test_users_and_login_as_test
      And `create a game` create_game(CreateGameRequest("test2", FieldSize(3, 2), "test2", 2))
      Then assert status.is(400)
    }

    Scenario("Returns 400 if 'size' x dimension is greater than 9") {
      When I create_test_users_and_login_as_test
      And `create a game` create_game(CreateGameRequest("test2", FieldSize(10, 3), "test2", 3))
      Then assert status.is(400)
    }

    Scenario("Returns 400 if 'size' y dimension is greater than 9") {
      When I create_test_users_and_login_as_test
      And `create a game` create_game(CreateGameRequest("test2", FieldSize(3, 10), "test2", 3))
      Then assert status.is(400)
    }

    Scenario("Returns 400 if 'crosses_length_to_win' is less than 3") {
      When I create_test_users_and_login_as_test
      And `create a game` create_game(CreateGameRequest("test2", FieldSize(3, 11), "test2", 2))
      Then assert status.is(400)
    }

    Scenario("Returns 400 if 'crosses_length_to_win' is greater than min(size(0), size(1)") {
      When I create_test_users_and_login_as_test
      And `create a game` create_game(CreateGameRequest("test2", FieldSize(5, 3), "test2", 4))
      Then assert status.is(400)
    }

    Scenario("Returns 400 if data does not meet the specification") {
      When I create_test_users_and_login_as_test
      When I post("/game").withBody("{\"a\":\"b\"}").withHeaders("session" -> "<session>")
      Then assert status.is(400)
    }

    Scenario("Returns 200 and created game if it's valid 1") {
      When I create_test_users_and_login_as_test
      And `create a game` create_game(CreateGameRequest("test2", FieldSize(3, 3), "test", 3))
      Then assert status.is(200)
      Then assert body.is(Game(1, Some("test"), None, finished = false, Seq("test", "test2"), 0, FieldSize(3,3), 3, Field.fill(3, 3)).asJson)
    }

    Scenario("Returns 200 and created game if it's valid 2") {
      When I create_test_users_and_login_as_test
      And `create a game` create_game(CreateGameRequest("test2", FieldSize(9, 9), "test", 9))
      Then assert status.is(200)
      Then assert body.is(Game(1, Some("test"), None, finished = false, Seq("test", "test2"), 0, FieldSize(9,9), 9, Field.fill(9, 9)).asJson)
    }

    Scenario("Returns 200 and created game if it's valid 3") {
      When I create_test_users_and_login_as_test
      And `create a game` create_game(CreateGameRequest("test2", FieldSize(9, 3), "test", 3))
      Then assert status.is(200)
      Then assert body.is(Game(1, Some("test"), None, finished = false, Seq("test", "test2"), 0, FieldSize(9,3), 3, Field.fill(9, 3)).asJson)
    }
  }
}
