package wtf.scala.coursetest

import com.github.agourlay.cornichon.CornichonFeature
import com.github.agourlay.cornichon.core.FeatureDef
import io.circe.syntax._

class GetGameIdSpec extends CornichonFeature with TicTacToeSpecCommon {

  def feature: FeatureDef = Feature("GET /game/:id") {

    Scenario("Returns 200 for getting existed game") {
      When I create_test_users_and_login_as_test
      And `create a game` create_game(CreateGameRequest("test2", FieldSize(3,3), "test2", 3))
      Then assert status.isSuccess
      Then `I can get it` get("/game/1")
      Then assert status.isSuccess
      Then assert body.is(Game(1, Some("test2"), None, finished = false, Seq("test", "test2"), 0, FieldSize(3,3), 3, Field.fill(3, 3)).asJson)
    }

    Scenario("Returns 404 for getting not existed game") {
      When I get("/game/1")
      Then assert status.is(404)
    }
  }
}