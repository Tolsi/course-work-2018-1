package wtf.scala.coursetest

import com.github.agourlay.cornichon.CornichonFeature
import com.github.agourlay.cornichon.core.FeatureDef
import scala.concurrent.duration._

class PostUserLoginSpec extends CornichonFeature with TicTacToeSpecCommon {

  def feature: FeatureDef = Feature("POST /user/login") {

    Scenario("Returns 200 with a valid session token for a valid credentials") {
      When I create_user(User("test", "q" * 8))
      Then assert status.isSuccess
      And `try to login` login(User("test", "q" * 8))
      Then assert status.isSuccess
      Then assert body.path("session").isPresent
    }

    Scenario("Returns 403 for empty password") {
      When I create_user(User("test", "q" * 8))
      Then assert status.isSuccess
      And `try to login` login(User("test", ""))
      Then assert status.is(403)
      Then assert body.path("session").isAbsent
    }

    Scenario("Any request with a session should prolong the session") {
      When I create_test_users_and_login_as_test_and_create_test_games
      Then I stepAs(User("test", "q" * 8), 1, Step(0, 0))
      Then aserts status.isSuccess
      Then I wait(6 seconds)
      Then I stepAs(User("test", "q" * 8), 2, Step(0, 0))
      Then aserts status.isSuccess
      Then I wait(6 seconds)
      Then I stepAs(User("test", "q" * 8), 3, Step(0, 0))
      Then aserts status.isSuccess
      Then I wait(6 seconds)
      Then I stepAs(User("test", "q" * 8), 4, Step(0, 0))
      Then aserts status.isSuccess
    }

    Scenario("Returns 403 for invalid password") {
      When I create_user(User("test", "q" * 8))
      Then assert status.isSuccess
      And `try to login` login(User("test", "v" * 8))
      Then assert status.is(403)
      Then assert body.path("session").isAbsent
    }

    Scenario("Returns 400 for invalid request") {
      When I post("/user/login").withBody("{\"a\": \"b\"}")
      Then assert status.is(400)
      Then assert body.path("session").isAbsent
    }
  }
}