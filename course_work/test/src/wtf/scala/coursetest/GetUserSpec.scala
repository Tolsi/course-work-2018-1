package wtf.scala.coursetest

import com.github.agourlay.cornichon.CornichonFeature
import com.github.agourlay.cornichon.core.FeatureDef
import io.circe.Json
import io.circe.syntax._
import wtf.scala.coursetest.TicTacToeJson._

class GetUserSpec extends CornichonFeature with TicTacToeSpecCommon {

  def feature: FeatureDef = Feature("/user?limit=:limit&offset=:offset") {

    Scenario("Returns 400 for limit <= 0") {
      When I get_users(0, 1)
      Then assert status.is(400)
    }

    Scenario("Returns 400 for offset < 0") {
      When I get_users(1, -1)
      Then assert status.is(400)
    }

    Scenario("Returns 200 and user data for offset = 0") {
      When I create_many_test_users
      Then `get first user` get_users(1, 0)
      Then assert status.is(200)
      Then assert body.is(Seq(UserStat("test", 0, 0, online = false).asJson).asJson)
    }

    Scenario("Returns 200 and user data for many objects") {
      When I create_many_test_users
      Then `get first user` get_users(5, 3)
      Then assert status.is(200)
      Then assert body.is((4 to 6).map(i => UserStat(s"test$i", 0, 0, online = false)).asJson)
    }
  }
}