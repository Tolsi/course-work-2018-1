package wtf.scala.coursetest

import com.github.agourlay.cornichon.CornichonFeature
import com.github.agourlay.cornichon.core.FeatureDef
import scala.concurrent.duration._

class PostUserLogoutSpec extends CornichonFeature with TicTacToeSpecCommon {

  def feature: FeatureDef = Feature("POST /user/logout") {

    Scenario("Returns 200 if session is active and it was closed") {
      When I create_user(User("test", "q" * 8))
      Then assert status.isSuccess
      And `try to login` login(User("test", "q" * 8))
      Then assert status.isSuccess
      Then `try to logout` logout
      Then assert status.isSuccess
    }

    Scenario("Returns 401 if session is expired") {
      When I create_user(User("test", "q" * 8))
      Then assert status.isSuccess
      And `try to login` login(User("test", "q" * 8))
      Then assert status.isSuccess
      Then `wait 11sec` wait(11 seconds)
      Then `try to logout` logout
      Then assert status.is(401)
    }

    Scenario("Returns 401 if there're no 'session' header") {
      When I create_user(User("test", "q" * 8))
      Then assert status.isSuccess
      And `try to login` login(User("test", "q" * 8))
      Then assert status.isSuccess
      Then `try to logout without session` post("/user/logout")
      Then assert status.is(401)
    }
  }
}