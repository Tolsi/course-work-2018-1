package wtf.scala.coursetest

import com.github.agourlay.cornichon.CornichonFeature
import com.github.agourlay.cornichon.core.FeatureDef
import io.circe.Json
import io.circe.syntax._

class GetGameSpec extends CornichonFeature with TicTacToeSpecCommon {

  def feature: FeatureDef = Feature("GET /game?limit=:limit&offset=:offset") {

    Scenario("Returns 400 for limit <= 0") {
      When I get_games(0, 1)
      Then assert status.is(400)
    }

    Scenario("Returns 400 for offset < 0") {
      When I get_games(1, -1)
      Then assert status.is(400)
    }

    Scenario("Returns 200 and game data for offset = 0") {
      When I create_test_users_and_login_as_test_and_create_test_games
      Then `get first game` get_games(1, 0)
      Then assert status.is(200)
      Then assert body.is(Seq(Game(1, Some("test"), None, finished = false, Seq("test", "test2"), 0, FieldSize(3, 3), 3, Field.fill(3,3))).asJson)
    }

    Scenario("Returns 200 and game data for many objects") {
      When I create_test_users_and_login_as_test_and_create_test_games
      Then `get first game` get_games(3, 5)
      Then assert status.is(200)
      Then assert body.is((6 to 8).map(i => Game(i, Some("test"), None, finished = false, Seq("test", "test2"), 0, FieldSize(3, 3), 3, Field.fill(3,3))).asJson)
    }
  }
}