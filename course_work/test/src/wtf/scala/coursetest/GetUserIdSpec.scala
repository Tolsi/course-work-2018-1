package wtf.scala.coursetest

import com.github.agourlay.cornichon.CornichonFeature
import com.github.agourlay.cornichon.core.FeatureDef
import io.circe.syntax._
import wtf.scala.coursetest.TicTacToeJson._
import scala.concurrent.duration._

class GetUserIdSpec extends CornichonFeature with TicTacToeSpecCommon {

  def feature: FeatureDef = Feature("GET /user/:id") {

    Scenario("Returns 200 for getting existed user") {
      When I create_user(User("test", "q" * 8))
      Then assert status.isSuccess
      Then `I can get it` get("/user/test")
      Then assert status.isSuccess
      Then assert body.is(UserStat("test", 0, 0, online = false).asJson)
    }

    Scenario("Returns 200 for getting existed user with online flag after first operation with token") {
      When I create_test_users_and_login_as_test
      Then assert status.isSuccess
      Then `I can get it` get("/user/test")
      Then assert body.is(UserStat("test", 0, 0, online = true).asJson)
      Then `I wait session timeout` wait(11 seconds)
      Then `I get it again` get("/user/test")
      Then assert body.is(UserStat("test", 0, 0, online = false).asJson)
    }

    Scenario("Returns 404 for getting not existed user") {
      When I get("/user/test")
      Then assert status.is(404)
    }
  }
}