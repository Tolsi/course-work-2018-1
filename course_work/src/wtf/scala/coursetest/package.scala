package wtf.scala

import io.circe.generic.extras.{Configuration, ConfiguredJsonCodec}

package object coursetest {
  import wtf.scala.coursetest.TicTacToeJson._

  implicit val jsonConfig: Configuration = Configuration.default.withSnakeCaseMemberNames

  case class Step(x: Byte, y: Byte)

  implicit def step2stepRequest(step: Step):StepRequest = StepRequest(step)

  case class StepRequest(step: Step)

  case class FieldSize(x: Byte, y: Byte)

  object Field {
    def fill(size: FieldSize): Field = fill(size.x, size.y)

    def fill(x: Byte, y: Byte): Field = Field(Seq.fill(y)(Seq.fill(x)(0)))
  }

  case class Field(seq: Seq[Seq[Byte]]) {
    lazy val steps: Int = seq.flatten.count(_ != 0)

    def step(step: Step, user: Byte): Field = {
      copy(seq.updated(step.y, seq(step.y).updated(step.x, user)))
    }

    lazy val isFull: Boolean = !seq.flatten.contains(0)

    def apply(x: Int, y: Int): Byte = seq(y)(x)
  }

  case class Session(session: String)

  @ConfiguredJsonCodec
  case class CreateGameRequest(opponent: String, size: FieldSize, firstStepBy: String, crossesLengthToWin: Byte)

  object Game {
    def findWinner(field: Field, crossesLengthToWin: Byte): Option[Byte] = {
      import field.seq
      val mayBeWinnerIsHere = for {
        (seq, y) <- seq.zipWithIndex
        (value, x) <- seq.zipWithIndex
        if value != 0
      } yield mayBeWinnerOnThisPoint(field, crossesLengthToWin, x, y) -> field(x, y)
      mayBeWinnerIsHere.find(_._1).map(_._2)
    }

    def mayBeWinnerOnThisPoint(field: Field, crossesLengthToWin: Byte, pointX: Int, pointY: Int): Boolean = {
      val xSize = field.seq.head.length
      val ySize = field.seq.length

      val player = field(pointX, pointY)

      val sameFromLeft = (pointX - 1 until 0 by -1).takeWhile(x => field(x, pointY) == player).size
      val sameFromRight = (pointX + 1 until xSize).takeWhile(x => field(x, pointY) == player).size

      val horisontalCount = sameFromLeft + sameFromRight + 1

      val sameFromUp = (pointY - 1 until 0 by -1).takeWhile(y => field(pointX, y) == player).size
      val sameFromDown = (pointY + 1 until ySize).takeWhile(y => field(pointX, y) == player).size

      val verticalCount = sameFromUp + sameFromDown + 1

      val sameFromFirstDiagonalUp = (1 until math.min(pointX, pointY)).takeWhile(i => field(pointX - i, pointY - i) == player).size
      val sameFromFirstDiagonalDown = (1 until math.min(xSize - pointX, ySize - pointY)).takeWhile(i => field(pointX + i, pointY + i) == player).size

      val firstDiagonalCount = sameFromFirstDiagonalUp + sameFromFirstDiagonalDown + 1

      val sameFromSecondDiagonalUp = (1 until math.min(xSize - pointX, pointY)).takeWhile(i => field(pointX + i, pointY - i) == player).size
      val sameFromSecondDiagonalDown = (1 until math.min(pointX, ySize - pointY)).takeWhile(i => field(pointX - i, pointY + i) == player).size

      val secondDiagonalCount = sameFromSecondDiagonalUp + sameFromSecondDiagonalDown + 1

      horisontalCount >= crossesLengthToWin ||
        verticalCount >= crossesLengthToWin ||
        firstDiagonalCount >= crossesLengthToWin ||
        secondDiagonalCount >= crossesLengthToWin
    }
  }

  @ConfiguredJsonCodec
  case class Game(id: Long, nextStep: Option[String], won: Option[String], finished: Boolean,
                  players: Seq[String], steps: Byte, size: FieldSize, crossesLengthToWin: Byte, field: Field) {
  import Game._

    def step(username: String, step: Step): Game = {
      val updatedField = field.step(step, (players.indexOf(username) + 1).toByte)
      val mayBeWinnerIndex = findWinner(updatedField, crossesLengthToWin)
      val newFinished = mayBeWinnerIndex.isDefined || updatedField.isFull
      val mayBeWinner = mayBeWinnerIndex.map(b => players(b - 1))
      val newNextStep = if (!newFinished) Some(players.filterNot(nextStep.contains).head) else None
      copy(nextStep = newNextStep, won = mayBeWinner, finished = newFinished, steps = updatedField.steps.toByte, field = updatedField)
    }
  }

  case class User(username: String, password: String)
  case class UserStat(username: String, wins: Int, losses: Int, online: Boolean)

}
