package wtf.scala.coursetest

import io.circe.generic.semiauto._
import io.circe.syntax._
import io.circe.{Decoder, Encoder, HCursor, Json}

object TicTacToeJson {
  implicit val encodeField: Encoder[Field] = (a: Field) => a.seq.asJson
  implicit val decodeField: Decoder[Field] = (c: HCursor) => for {
    array <- c.downArray.as[Seq[Seq[Byte]]]
  } yield Field(array)

  class ArrayEncoder[T](to: (Byte, Byte) => T, from: T => (Byte, Byte)) extends Encoder[T] {
    override def apply(a: T): Json = {
      val (x, y) = from(a)
      Json.arr(Json.fromInt(x), Json.fromInt(y))
    }
  }

  class ArrayDecoder[T](to: (Byte, Byte) => T, from: T => (Byte, Byte)) extends Decoder[T] {
    final def apply(c: HCursor): Decoder.Result[T] = {
      for {
        array <- c.as[Seq[Byte]]
      } yield to(array(0), array(1))
    }
  }

  implicit val encodeStep: Encoder[Step] = new ArrayEncoder[Step](Step.apply, (s: Step) => (s.x, s.y))
  implicit val decodeStep: Decoder[Step] = new ArrayDecoder[Step](Step.apply, (s: Step) => (s.x, s.y))

  implicit val encodeFieldSize: Encoder[FieldSize] = new ArrayEncoder[FieldSize](FieldSize.apply, (s: FieldSize) => (s.x, s.y))
  implicit val decodeFieldSize: Decoder[FieldSize] = new ArrayDecoder[FieldSize](FieldSize.apply, (s: FieldSize) => (s.x, s.y))

  implicit val decodeUser: Decoder[User] = deriveDecoder
  implicit val encodeUser: Encoder[User] = deriveEncoder

  implicit val decodeSession: Decoder[Session] = deriveDecoder
  implicit val encodeSession: Encoder[Session] = deriveEncoder

  implicit val decodeStepRequest: Decoder[StepRequest] = deriveDecoder
  implicit val encodeStepRequest: Encoder[StepRequest] = deriveEncoder

  implicit val decodeUserStat: Decoder[UserStat] = deriveDecoder
  implicit val encodeUserStat: Encoder[UserStat] = deriveEncoder

}