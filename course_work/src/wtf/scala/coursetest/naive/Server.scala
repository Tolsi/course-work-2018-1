package wtf.scala.coursetest.naive

import akka.http.scaladsl.server.{HttpApp, Route}
import wtf.scala.coursetest.naive.routes.{DebugRoute, GameRoute, UserRoute}
import wtf.scala.coursetest.naive.storage.mock.{MockGameRepository, MockSessionRepository, MockUserRepository}

object Server extends HttpApp {
  val mockGameRepository = new MockGameRepository
  val mockSessionRepository = new MockSessionRepository
  val mockUserRepository = new MockUserRepository(mockSessionRepository)

  val debug = new DebugRoute(mockSessionRepository, mockGameRepository, mockUserRepository).route
  val game = new GameRoute(mockSessionRepository, mockGameRepository, mockUserRepository).route
  val user = new UserRoute(mockUserRepository, mockSessionRepository).route

  override protected def routes: Route = game ~ user ~ debug

  def main(args: Array[String]): Unit = {
    startServer("0.0.0.0", 8080)
  }
}
