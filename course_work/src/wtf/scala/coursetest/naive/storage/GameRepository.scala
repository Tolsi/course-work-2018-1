package wtf.scala.coursetest.naive.storage

import wtf.scala.coursetest.{Game, Step}

import scala.concurrent.Future

trait GameRepository {
  def createGame(game: Game): Future[Game]
  def getGame(id: Long): Future[Option[Game]]
  def getGames(offset: Int, limit: Int): Future[Seq[Game]]
  def step(game: Game, username: String, step: Step): Future[Game]

  def drop(): Future[Unit]
}
