package wtf.scala.coursetest.naive.storage

import wtf.scala.coursetest.{Session, User}

import scala.concurrent.Future

trait SessionRepository {
  def createSession(user: User): Future[Session]
  def finishSession(session: Session): Future[Boolean]
  def getUserBySession(session: Session): Future[Option[User]]
  def getLastSeenTimeByUsername(username: String): Future[Option[Long]]
  def prolongSession(session: Session): Future[Unit]

  def drop(): Future[Unit]
}
