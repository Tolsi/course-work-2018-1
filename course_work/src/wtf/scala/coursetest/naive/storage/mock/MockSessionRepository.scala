package wtf.scala.coursetest.naive.storage.mock

import java.util.UUID

import wtf.scala.coursetest
import wtf.scala.coursetest.{Session, User}
import wtf.scala.coursetest.naive.storage.SessionRepository

import scala.collection.mutable
import scala.concurrent.Future
import scala.concurrent.duration._

class MockSessionRepository extends SessionRepository {
  private val sessions: mutable.Map[Session, (User, Long)] = new mutable.HashMap[Session, (User, Long)]()

  override def createSession(user: coursetest.User): Future[coursetest.Session] = {
    // remove previous opened user sessions
    sessions.filter(_._2._1 == user).keys.foreach(sessions.remove)
    val session = Session(UUID.randomUUID().toString)
    sessions += session -> (user, System.currentTimeMillis())
    Future.successful(session)
  }

  private def _prolongSession(session: coursetest.Session): Unit = {
    sessions += session -> sessions(session).copy(_2 = System.currentTimeMillis())
  }

  private def findValidSession(session: coursetest.Session, timeoutMillis: Long = 10.seconds.toMillis, prolong: Boolean = false): Option[User] = {
    sessions.get(session)
      .filter(userAndTime => {
        val isValid = userAndTime._2 + timeoutMillis > System.currentTimeMillis()
        if (isValid) {
          if (prolong) _prolongSession(session)
          true
        } else {
          sessions.remove(session)
          false
        }
      })
      .map(_._1)
  }

  override def finishSession(session: coursetest.Session): Future[Boolean] = Future.successful(findValidSession(session).exists(_ => {
    sessions.remove(session)
    true
  }))

  override def getUserBySession(session: coursetest.Session): Future[Option[coursetest.User]] = {
    Future.successful(findValidSession(session))
  }

  override def prolongSession(session: coursetest.Session): Future[Unit] = Future.successful(_prolongSession(session))

  override def drop(): Future[Unit] = Future.successful(sessions.clear())

  override def getLastSeenTimeByUsername(username: String): Future[Option[Long]] = Future.successful {
    sessions.find(_._2._1.username == username).map(_._2._2)
  }
}
