package wtf.scala.coursetest.naive.storage.mock

import java.util.concurrent.atomic.AtomicLong

import wtf.scala.coursetest._
import wtf.scala.coursetest.naive.storage.GameRepository

import scala.collection.mutable
import scala.concurrent.Future

class MockGameRepository extends GameRepository {
  private val idGen = new AtomicLong(1)
  private val games: mutable.SortedMap[Long, Game] = new mutable.TreeMap[Long, Game]()

  override def createGame(game: Game): Future[Game] = {
    val gameWithId = game.copy(id = idGen.getAndIncrement())
    games += gameWithId.id -> gameWithId
    Future.successful(gameWithId)
  }

  override def getGame(id: Long): Future[Option[Game]] = Future.successful(games.get(id))

  override def getGames(offset: Int, limit: Int): Future[Seq[Game]] = Future.successful(games.values.toSeq.slice(offset, offset + limit))

  override def step(game: Game, username: String, step: Step): Future[Game] = Future.successful {
    val updatedGame = game.step(username, step)
    games += game.id -> updatedGame
    updatedGame
  }

  override def drop(): Future[Unit] = Future.successful {
    games.clear()
    idGen.set(1)
  }
}
