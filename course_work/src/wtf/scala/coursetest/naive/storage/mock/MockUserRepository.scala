package wtf.scala.coursetest.naive.storage.mock

import wtf.scala.coursetest
import wtf.scala.coursetest.naive.storage.{SessionRepository, UserRepository}
import wtf.scala.coursetest.{User, UserStat}

import scala.collection.mutable
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.concurrent.duration._

class MockUserRepository(override val sessionRepository: SessionRepository) extends UserRepository {
  private val users: mutable.SortedMap[String, User] = new mutable.TreeMap[String, User]()
  private val usersStat: mutable.HashMap[String, UserStat] = new mutable.HashMap[String, UserStat]()
  private val usersLastOnline: mutable.HashMap[String, Long] = new mutable.HashMap[String, Long]()

  override def createUser(user: User): Future[Boolean] = Future.successful(if (users.contains(user.username)){
    false
  } else {
    users += user.username -> user
    true
  })

  override def getUser(username: String): Future[Option[User]] = Future.successful(users.get(username))

  override def getUsers(offset: Int, limit: Int): Future[Seq[User]] = Future.successful(users.values.toSeq.slice(offset, offset + limit))

  override def drop(): Future[Unit] = Future.successful {
    users.clear()
  }

  override def getUserStat(username: String): Future[coursetest.UserStat] = {
    isOnline(username).map(isOnlineValue =>
      usersStat.getOrElse(username, UserStat(username, 0,0, online = false))
        .copy(online = isOnlineValue))
  }

  private def isOnline(username: String): Future[Boolean] = sessionRepository.getLastSeenTimeByUsername(username).map { lastSeenTimeOpt =>
    lastSeenTimeOpt.exists(l => l + 10.seconds.toMillis > System.currentTimeMillis())
  }

  private def updateUserStatField(username: String, update: UserStat => UserStat): Unit = {
    val userStat = usersStat.getOrElse(username, UserStat(username, 0, 0, online = false))
    val updatedUserStat = update(userStat)
    usersStat.update(username, updatedUserStat)
  }

  override def addWin(username: String): Future[Unit] = Future.successful {
    updateUserStatField(username, userStat => userStat.copy(wins = userStat.wins + 1))
  }

  override def addLose(username: String): Future[Unit] = Future.successful {
    updateUserStatField(username, userStat => userStat.copy(losses = userStat.losses + 1))
  }

  override def getUserStats(offset: Int, limit: Int): Future[Seq[UserStat]] = {
    getUsers(offset, limit).map(_.map(_.username)).flatMap(usernames => Future.sequence(usernames.map(getUserStat)))
  }
}
