package wtf.scala.coursetest.naive.storage

import wtf.scala.coursetest.{User, UserStat}

import scala.concurrent.Future

trait UserRepository {
  def sessionRepository: SessionRepository

  def createUser(user: User): Future[Boolean]
  def getUser(username: String): Future[Option[User]]
  def getUserStat(username: String): Future[UserStat]
  def getUserStats(offset: Int, limit: Int): Future[Seq[UserStat]]
  def getUsers(offset: Int, limit: Int): Future[Seq[User]]
  def addWin(username: String): Future[Unit]
  def addLose(username: String): Future[Unit]

  def drop(): Future[Unit]
}
