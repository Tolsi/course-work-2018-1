package wtf.scala.coursetest.naive.routes

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server._
import wtf.scala.coursetest.naive.storage.SessionRepository
import wtf.scala.coursetest.{Session, User}

trait TicTacToeDirectives extends Directives {
  def sessionRepository: SessionRepository

  def withActiveSession: Directive1[(Session, User)] = {
      optionalHeaderValueByName('session).flatMap {
        case Some(session) =>
          val s = Session(session)
          onSuccess(sessionRepository.getUserBySession(s)).flatMap {
            case Some(user) =>
              onSuccess(sessionRepository.prolongSession(s)).tflatMap(_ => provide(s, user))
            case None =>
              complete(StatusCodes.Unauthorized -> "This session is not active")
          }
        case None =>
          complete(StatusCodes.Unauthorized -> "Missing 'session' header")
      }
  }
}
