package wtf.scala.coursetest.naive.routes

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Route
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport
import wtf.scala.coursetest.TicTacToeJson._
import wtf.scala.coursetest.User
import wtf.scala.coursetest.naive.storage.{SessionRepository, UserRepository}

class UserRoute(userRepository: UserRepository, override val sessionRepository: SessionRepository) extends TicTacToeRoute {
  val route: Route = pathPrefix("user") {
    path("login") {
      post {
        entity(as[User]) { user =>
          onSuccess(userRepository.getUser(user.username)) {
            case Some(dbUser) => if (dbUser == user) {
              onSuccess(sessionRepository.createSession(user)) { session =>
                complete(session)
              }
            } else {
              complete(StatusCodes.Forbidden)
            }
            case None => complete(StatusCodes.Forbidden)
          }
        }
      }
    } ~ path("logout") {
      post {
        withActiveSession { case (session, _) =>
          onSuccess(sessionRepository.finishSession(session)) { result =>
            complete {
              if (result) StatusCodes.OK else StatusCodes.Unauthorized
            }
          }
        }
      }
    } ~ path(Segment) { username =>
      get {
        onSuccess(userRepository.getUser(username)) {
          case Some(user) => complete(userRepository.getUserStat(username))
          case None => complete(StatusCodes.NotFound)
        }
      }
    } ~ post {
      entity(as[User]) { user =>
        if (user.username.length >= 3 && user.username.length < 20 &&
          user.password.length >= 8 && user.password.length < 100) {
          onSuccess(userRepository.createUser(user)) { result =>
            complete {
              if (result) {
                StatusCodes.OK
              } else {
                StatusCodes.Conflict
              }
            }
          }
        } else {
          complete(StatusCodes.BadRequest)
        }
      }
    } ~ get {
      parameters('limit.as[Int], 'offset.as[Int]) { (limit, offset) =>
        onSuccess(userRepository.getUserStats(offset, limit)) { users =>
          if (limit > 0 && offset >= 0) {
            complete(users)
          } else {
            complete(StatusCodes.BadRequest)
          }
        }
      }
    }
  }
}
