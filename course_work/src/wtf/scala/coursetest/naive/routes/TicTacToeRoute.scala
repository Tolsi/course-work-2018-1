package wtf.scala.coursetest.naive.routes

import akka.http.scaladsl.server.{Directives, Route}
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport

abstract class TicTacToeRoute extends Directives with FailFastCirceSupport with TicTacToeDirectives {
  def route: Route
}
