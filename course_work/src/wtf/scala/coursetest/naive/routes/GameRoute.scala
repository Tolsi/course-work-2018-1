package wtf.scala.coursetest.naive.routes

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.{Directive1, Route}
import wtf.scala.coursetest.TicTacToeJson._
import wtf.scala.coursetest.naive.storage.{GameRepository, SessionRepository, UserRepository}
import wtf.scala.coursetest.{CreateGameRequest, Field, Game, StepRequest}
import io.circe.generic.auto._

class GameRoute(override val sessionRepository: SessionRepository, gameRepository: GameRepository, userRepository: UserRepository) extends TicTacToeRoute {

  def withExistedGame(gameId: Long): Directive1[Game] = {
    onSuccess(gameRepository.getGame(gameId)).flatMap {
      case Some(game) => provide(game)
      case None => complete(StatusCodes.NotFound)
    }
  }

  val route: Route = pathPrefix("game") {
    path(LongNumber) { id =>
      post {
        withExistedGame(id) { game =>
          withActiveSession { case (session, user) =>
            entity(as[StepRequest]) { stepRequest =>
              if (game.players.contains(user.username)) {
                val isBadRequest = !game.nextStep.contains(user.username) ||
                  game.finished || stepRequest.step.x >= game.size.x || stepRequest.step.x < 0 ||
                  stepRequest.step.y >= game.size.y || stepRequest.step.y < 0

                if (isBadRequest) {
                  complete(StatusCodes.BadRequest)
                } else {
                  val isConflict = game.field(stepRequest.step.x, stepRequest.step.y) != 0
                  if (isConflict) {
                    complete(StatusCodes.Conflict)
                  } else {
                    onSuccess(gameRepository.step(game, user.username, stepRequest.step)) {
                      updatedGame => complete(updatedGame)
                    }
                  }
                }
              } else {
                complete(StatusCodes.Forbidden)
              }
            }
          }
        }
      } ~ get {
        withExistedGame(id) { game =>
          complete(game)
        }
      }
    } ~ post {
      withActiveSession { case (session, user) =>
        entity(as[CreateGameRequest]) { createGameRequest =>
          import createGameRequest._
          val isBadRequest =
            size.x > 9 || size.x < 3 ||
              size.y > 9 || size.y < 3 ||
              crossesLengthToWin < 3 || crossesLengthToWin > math.min(size.x, size.y) ||
              (firstStepBy != user.username && firstStepBy != createGameRequest.opponent) ||
              user.username == createGameRequest.opponent
          if (isBadRequest) {
            complete(StatusCodes.BadRequest)
          } else {
            onSuccess(userRepository.getUser(createGameRequest.opponent)) {
              case Some(_) =>
                val game = Game(id = -1, nextStep = Some(firstStepBy), won = None, finished = false,
                  Seq(user.username, opponent), steps = 0, size, crossesLengthToWin, Field.fill(size))
                onSuccess(gameRepository.createGame(game)) { game =>
                  complete(game)
                }
              case None => complete(StatusCodes.BadRequest)
            }
          }
        }
      }
    } ~ get {
      parameters('limit.as[Int], 'offset.as[Int]) { (limit, offset) =>
        onSuccess(gameRepository.getGames(offset, limit)) { games =>
          if (limit > 0 && offset >= 0) {
            complete(games)
          } else {
            complete(StatusCodes.BadRequest)
          }
        }
      }
    }
  }

}
