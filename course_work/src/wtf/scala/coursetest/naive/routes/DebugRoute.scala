package wtf.scala.coursetest.naive.routes

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.{Directives, Route}
import wtf.scala.coursetest.naive.storage.{GameRepository, SessionRepository, UserRepository}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class DebugRoute(sessionRepository: SessionRepository, gameRepository: GameRepository, userRepository: UserRepository) extends Directives {
  val route: Route = pathPrefix("debug") {
    path("reset") {
      post {
        optionalHeaderValueByName('admin) { adminHeader =>
          if (adminHeader.isDefined) {
            onSuccess(Future.sequence(Seq(sessionRepository.drop(), gameRepository.drop(), userRepository.drop()))) { _ =>
              complete(StatusCodes.OK)
            }
          } else {
            complete(StatusCodes.Unauthorized)
          }
        }
      }
    }
  }
}
